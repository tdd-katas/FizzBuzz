package com.tdd.kata.utils;

import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import org.junit.runner.RunWith;

import java.util.function.IntPredicate;

import static org.junit.Assert.assertTrue;

@RunWith(ZohhakRunner.class)
public class FizzBuzzPredicatesTest {

    private IntPredicate sut;

    @TestWith({"15", "45", "60"})
    public void shouldMatchMultiplesOf15(int value) throws Exception {
        //Given
        sut = FizzBuzzPredicates.isMultipleOf(15);

        //When
        boolean result = sut.test(value);

        //Then
        assertTrue(result);
    }

    @TestWith({ "5", "10", "50", "65"})
    public void shouldMatchMultiplesOf5(int value) throws Exception {
        //Given
        sut = FizzBuzzPredicates.isMultipleOf(5);

        //When
        boolean result = sut.test(value);

        //Then
        assertTrue(result);
    }

    @TestWith({"3", "9", "33", "66"})
    public void shouldMatchMultiplesOf3(int value) throws Exception {
        //Given
        sut = FizzBuzzPredicates.isMultipleOf(3);

        //When
        boolean result = sut.test(value);

        //Then
        assertTrue(result);
    }

}
