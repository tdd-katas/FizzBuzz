package com.tdd.kata.utils;

import com.tdd.kata.exception.FizzBuzzException;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;

import static com.tdd.kata.utils.FizzBuzzPredicates.*;
import static com.tdd.kata.utils.FizzBuzzFunctions.*;

public class FizzBuzzConverter {

    private static List<Converter> converters = Arrays.asList(
            new Converter(isMultipleOf(15), fizzBuzz()),
            new Converter(isMultipleOf(5), buzz()),
            new Converter(isMultipleOf(3), fizz())
    );

    public String convert(int input){
        validate(input);
        return converters.stream()
                .filter(converter -> converter.getPredicate().test(input))
                .findFirst().orElse(new Converter(convertToString()))
                .getFunction().apply(input);
    }

    private void validate(int input) {
        if (input < 1) {
            throw new FizzBuzzException("Input value below 1");
        }

        if (input > 100) {
            throw new FizzBuzzException("Input value above 100");
        }
    }

    private static class Converter {

        private IntPredicate predicate;
        private IntFunction<String> function;

        Converter(IntFunction<String> function) {
            this.function = function;
        }
        Converter(IntPredicate predicate, IntFunction<String> function) {
            this.predicate = predicate;
            this.function = function;
        }

        IntPredicate getPredicate() {
            return predicate;
        }

        IntFunction<String> getFunction() {
            return function;
        }
    }
}
