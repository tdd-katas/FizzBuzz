package com.tdd.kata.utils;

import java.util.function.IntPredicate;

class FizzBuzzPredicates {

    static IntPredicate isMultipleOf(int v){return i -> i % v == 0;}
}
