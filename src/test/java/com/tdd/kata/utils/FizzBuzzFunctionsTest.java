package com.tdd.kata.utils;

import org.junit.Test;

import java.util.function.IntFunction;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;

public class FizzBuzzFunctionsTest {

    private IntFunction<String> sut;

    @Test
    public void shouldReturnFizzBuzz() throws Exception {
        //Given
        sut = FizzBuzzFunctions.fizzBuzz();

        //When
        String result = sut.apply(anyInt());

        //Then
        assertEquals("FizzBuzz", result);
    }

    @Test
    public void shouldReturnFizz() throws Exception {
        //Given
        sut = FizzBuzzFunctions.fizz();

        //When
        String result = sut.apply(anyInt());

        //Then
        assertEquals("Fizz", result);
    }


    @Test
    public void shouldReturnBuzz() throws Exception {
        //Given
        sut = FizzBuzzFunctions.buzz();

        //When
        String result = sut.apply(anyInt());

        //Then
        assertEquals("Buzz", result);
    }

    @Test
    public void shouldConvertNumberToString() throws Exception {
        //Given
        sut = FizzBuzzFunctions.convertToString();
        int value = 12345;

        //When
        String result = sut.apply(value);

        //Then
        assertEquals("12345", result);
    }
}
