package com.tdd.kata.utils;

import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import com.tdd.kata.exception.FizzBuzzException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(ZohhakRunner.class)
public class FizzBuzzConverterTest {

    private FizzBuzzConverter sut;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        sut = new FizzBuzzConverter();
    }

    @TestWith({"0", "-1", "-100"})
    public void shouldNotAcceptInputValuesBellow1(int input) throws Exception {
        //When
        expected.expect(FizzBuzzException.class);
        expected.expectMessage("Input value below 1");
        sut.convert(input);
    }

    @TestWith({"101", "150", "12345"})
    public void shouldNotAcceptInputValuesAbove100(int input) throws Exception {
        //When
        expected.expect(FizzBuzzException.class);
        expected.expectMessage("Input value above 100");
        sut.convert(input);
    }

    @TestWith({
            "1, 1",
            "3, Fizz",
            "6, Fizz",
            "5, Buzz",
            "10, Buzz",
            "15, FizzBuzz",
            "45, FizzBuzz",
            "100, Buzz"})
    public void shouldConvertInputNumberAccordingToBusinessRules(int value, String expectedResult) throws Exception {
        //Given
        FizzBuzzConverter sut = new FizzBuzzConverter();

        //When
        String result = sut.convert(value);

        //Then
        assertEquals(expectedResult, result);
    }
}
