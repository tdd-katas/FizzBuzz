package com.tdd.kata;

import com.tdd.kata.utils.FizzBuzzConverter;

import java.util.stream.IntStream;
import static java.util.stream.Collectors.joining;

public class FizzBuzz {

    private FizzBuzzConverter converter = new FizzBuzzConverter();

    String print() {
        return IntStream.rangeClosed(1, 100).mapToObj(converter::convert).collect(joining(" "));
    }

    public static void main(String[] args) {
        System.out.println(new FizzBuzz().print());
    }
}
