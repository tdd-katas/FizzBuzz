package com.tdd.kata.utils;

import java.util.function.IntFunction;

class FizzBuzzFunctions {

    static IntFunction<String> fizzBuzz() {return i -> "FizzBuzz";}

    static IntFunction<String> fizz() {return i -> "Fizz";}

    static IntFunction<String> buzz() {return i -> "Buzz";}

    static IntFunction<String> convertToString() {return Integer::toString;}
}