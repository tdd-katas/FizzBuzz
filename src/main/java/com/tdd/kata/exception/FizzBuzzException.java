package com.tdd.kata.exception;

public class FizzBuzzException extends RuntimeException {

    public FizzBuzzException(String message) {
        super(message);
    }
}
