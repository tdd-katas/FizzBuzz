package com.tdd.kata;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class FizzBuzzTest {

    private FizzBuzz sut;

    @Before
    public void setUp() throws Exception {
        sut = new FizzBuzz();
    }

    @Test
    public void shouldPrintFizzBuzzSequence() throws Exception {
        //When
        String result = sut.print();
        String[] fizzBuzzArray = result.split(" ");

        //Then
        assertEquals(100, fizzBuzzArray.length);
        assertEquals(27, Arrays.stream(fizzBuzzArray).filter(el -> el.equals("Fizz")).count());
        assertEquals(14, Arrays.stream(fizzBuzzArray).filter(el -> el.equals("Buzz")).count());
        assertEquals(6, Arrays.stream(fizzBuzzArray).filter(el -> el.equals("FizzBuzz")).count());
    }
}